#!/bin/bash
ENV_FILE_NAME=".env"
SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"

if [ -e "${ENV_FILE_NAME}" ]
then
  echo -n "You've already installed the scripts. Do you want to re-install it? Y/n: "
  read CONFIRM_ANSWER_RE_INSTALL
  if [ ! "${CONFIRM_ANSWER_RE_INSTALL}" = "Y" ]
  then
    exit
  fi
fi

echo -n "Enter Your Current Workspace Path [Ex. /Users/example/Desktop/WP_Unity]: "
read WORKSPACE_PATH

echo -n "Enter Your Product Name [WWE Champions] "
read TMP_PRODUCT_NAME

PRODUCT_NAME="WWE Champions"
if [ ! "${TMP_PRODUCT_NAME}" = "" ]
then
  PRODUCT_NAME="${TMP_PRODUCT_NAME}"
fi

echo -n "Enter Your Company Name [Scopely] "
read TMP_COMPANY_NAME

COMPANY_NAME="Scopely"
if [ ! "${TMP_COMPANY_NAME}" = "" ]
then
  COMPANY_NAME="${TMP_COMPANY_NAME}"
fi

echo -n "Enter Sublime Text Editor Path [/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl]:  "
read TMP_SUBLIME_BIN_PATH

SUBLIME_BIN_PATH="/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl"
if [ ! "${TMP_SUBLIME_BIN_PATH}" = "" ]
then
  SUBLIME_BIN_PATH="${TMP_SUBLIME_BIN_PATH}"
fi

echo -e "\n===CONFIRM YOUR CONFIGURATION==="
echo "Current Workspace: ${WORKSPACE_PATH}"
echo "Product Name: ${PRODUCT_NAME}"
echo "Company Name: ${COMPANY_NAME}"
echo "Sublime Bin Path: ${SUBLIME_BIN_PATH}"
echo "====================================="

echo -en "\nIs this correct? [Y]: "
read CONFIRM_ANSWER_INSTALL

if [ "${CONFIRM_ANSWER_INSTALL}" = "Y" ] || [ "${CONFIRM_ANSWER_INSTALL}" = "" ]
then
  if [ -e "${ENV_FILE_NAME}" ]
  then
    /bin/rm "${ENV_FILE_NAME}"
  fi
  echo "#!/bin/bash" >> "${ENV_FILE_NAME}"
  echo "CURRENT_WORKSPACE=\"${WORKSPACE_PATH}\"" >> "${ENV_FILE_NAME}"
  echo "BLUEPRINT_EDITOR=\"${SUBLIME_BIN_PATH}\"" >> "${ENV_FILE_NAME}"
  echo "PRODUCT_NAME=\"${PRODUCT_NAME}\"" >> "${ENV_FILE_NAME}"
  echo "COMPANY_NAME=\"${COMPANY_NAME}\"" >> "${ENV_FILE_NAME}"

  echo 'SCOPELY_CHAMPIONS_DST_FILE="/Users/$(whoami)/Library/Caches/Unity/"' >> "${ENV_FILE_NAME}"
  echo 'CHAMPIONS_DST_FILE="/Users/$(whoami)/Library/Application Support/${COMPANY_NAME}/"' >> "${ENV_FILE_NAME}"
  echo 'PREFAB_SETTING_FILE=${CURRENT_WORKSPACE}/Assets/StreamingAssets/prefab_settings.json' >> "${ENV_FILE_NAME}"
  echo "PREFAB_FILE_NAME=\$(cat \${PREFAB_SETTING_FILE} | grep file_name | cut -d ':' -f2 | tr -d , | xargs)" >> "${ENV_FILE_NAME}"
  echo 'PREFAB_FILE_DIR=${CURRENT_WORKSPACE}/Assets/StreamingAssets/Prefabs/${PREFAB_FILE_NAME}' >> "${ENV_FILE_NAME}"

  # Install Environment
  COUNT_CONFIGURATION_LINE=`cat ~/.zshrc | grep "${SCRIPT_DIR}" | wc -l`
  if [ "${COUNT_CONFIGURATION_LINE}" -le 1 ]
  then
    echo "export PATH=\$PATH:${SCRIPT_DIR}" >> ~/.zshrc
  fi
  
  echo "Successfully installed. Let's restart your terminal!"
fi
