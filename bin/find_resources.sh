#!/bin/bash
# Loading configuration
source .env

WORD_TO_FIND=${1}

# Find in the prefab settings
FOUND_LOCATION=`cat $PREFAB_FILE_DIR | python -m json.tool | grep ${WORD_TO_FIND} | xargs`
DID_FOUND=`echo -n $FOUND_LOCATION | wc -m`
if [ $DID_FOUND -gt 1 ]
then
  echo "Found: $PREFAB_FILE_DIR"
fi

# Find in the blueprints
cd "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/Contents/"
if [ ! -d "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/Contents/blueprints" ]; then
  unzip -q "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/Contents/blueprints.zip"
fi

find "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/Contents" -type f | while read f
do
  FOUND_LOCATION=`cat "$f" | grep -Eo "$WORD_TO_FIND"`
  DID_FOUND=`echo -n $FOUND_LOCATION | wc -m`
  if [ $DID_FOUND -gt 1 ]
  then
    echo "Found: $f"
  fi
done

# Find in the project
find ${CURRENT_WORKSPACE}/Assets/ -type f | grep -E ".prefab$|.cs$|.unity$" | while read f
do
  FOUND_LOCATION=`cat "$f" | grep -Eo "$WORD_TO_FIND"`
  DID_FOUND=`echo -n $FOUND_LOCATION | wc -m`
  if [ $DID_FOUND -gt 1 ]
  then
    echo "Found: $f"
  fi
done