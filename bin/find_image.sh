#!/bin/bash
source .env

PACKING_TAG="${1}"

echo "Dang ti\`m..."
cd "${CURRENT_WORKSPACE}"

find Assets/_hirez Assets/_lowrez Assets/_mhirez -type f | grep -E ".jpg.meta$|.png.meta$|.PNG.meta" | while IFS= read -r META_FILE
do
  COUNT_MATCH_TAG=`cat "${META_FILE}" | grep -E "spritePackingTag: ${PACKING_TAG}\$|spritePackingTag: low_${PACKING_TAG}\$|spritePackingTag: mhirez_${PACKING_TAG}\$" | wc -l`
  if [ "${COUNT_MATCH_TAG}" -eq "1" ]
  then
    echo "${META_FILE}"
  fi
done
echo "Done!"