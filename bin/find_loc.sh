#!/bin/bash
# Loading configuration
source .env

LOC_TO_FIND="${1}"

cd "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/Contents/"
if [ ! -d "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/Contents/loc/blueprints" ]; then
  unzip -q "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/Contents/localization.zip" -d loc
fi

cat "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/Contents/loc/blueprints/db_Localization.json" | sed 's/\\"/\"/g' | sed 's/\\n/ /g' | sed -e 's/\<\/entry\>/\<\/entry\>\
/g' | sed 's/^[\t ]*//g' | grep "${LOC_TO_FIND}"

/bin/rm -rf "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/Contents/loc"