#!/bin/bash
# Loading configuration
source .env
OPTION="${1}"

# Declare data
SERVER_URL=`cat "${CURRENT_WORKSPACE}/Assets/StreamingAssets/server_settings.json" | jq -r '.server_url'`
SERVER_CONFIG="userslots__"`echo "${SERVER_URL}" | cut -d'.' -f1 | cut -d'/' -f3`
USER_SLOTS_DIR="${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/$SERVER_CONFIG"
SELECTED_SLOT_INDEX=`cat "$USER_SLOTS_DIR" | jq -r ".selectedIndex"`
SLOT_CONTENT=`cat "$USER_SLOTS_DIR" | jq -r ".userSlots[$SELECTED_SLOT_INDEX]"`
USER_ID=`echo "$SLOT_CONTENT" | jq -r ".playerFolder" | tr -d p_`

# Get Username
USER_DATA_PLAIN_CONTEXT_DIR="${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/p_${USER_ID}/game.plaintext.json"
USER_NAME=`cat "${USER_DATA_PLAIN_CONTEXT_DIR}" | jq -r ".player_data.client_data" | jq -r ".PlayerData.MultiplayerPlayerName"`
DEVICE_ID=`echo "$SLOT_CONTENT" | jq -r ".playerName"`
RESULT=`echo -e "---\nSERVER: ${SERVER_URL}\nUSERNAME: "${USER_NAME}"\nUSER_ID: ${USER_ID} \nDEVICE_ID: ${DEVICE_ID}"`

# Show Result
echo "$RESULT"