#!/bin/bash
# Loading configuration
source .env

PACKING_TAG="${1}"

TMP_RESOURCES_FILE_NAME=${HOME}"/resources_file_name_${PACKING_TAG}.txt"
TMP_PREFABS_FILES_NAME=${HOME}"/resources_prefab_files_${PACKING_TAG}.txt"
TMP_PREFABS_RESULT_FILES_NAME=${HOME}"/prefab_result_${PACKING_TAG}.txt"

if [ -e "${TMP_RESOURCES_FILE_NAME}" ]
then
  /bin/rm "${TMP_RESOURCES_FILE_NAME}"
fi

if [ -e "${TMP_PREFABS_FILES_NAME}" ]
then
  /bin/rm "${TMP_PREFABS_FILES_NAME}"
fi

if [ -e "${TMP_PREFABS_RESULT_FILES_NAME}" ]
then
  /bin/rm "${TMP_PREFABS_RESULT_FILES_NAME}"
fi

find "${CURRENT_WORKSPACE}/Assets/_hirez/Resources" "${CURRENT_WORKSPACE}/Assets/_hirez/Prefabs" -type f -iname "*.prefab" > "${TMP_PREFABS_FILES_NAME}"
echo "Find all images with packing: ${PACKING_TAG}"
find "${CURRENT_WORKSPACE}/Assets/_hirez/" -type f -name "*.meta" | grep -E "Resources|Textures|UISprites|UITextures_POT_NoAlpha" | grep -vE ".prefab" | while read f
do
  COUNT_PACKING_TAG=`cat "$f" | grep "${PACKING_TAG}" | wc -l`
  if [ "${COUNT_PACKING_TAG}" -ge 1 ]
  then
    FILE_GUID=`cat "${f}" | grep "guid" | cut -d' ' -f2`
    echo "${f}:${FILE_GUID}" >> "${TMP_RESOURCES_FILE_NAME}"
  fi
done
cat "${TMP_RESOURCES_FILE_NAME}" | while read img_file
do
  FILE=`echo "${img_file}" | cut -d':' -f1`
  GUID=`echo "${img_file}" | cut -d':' -f2`

  echo "CHECKING: ${FILE}"
  cat "${TMP_PREFABS_FILES_NAME}" | while read prefab_file
  do
    COUNT_USED_FILE=`cat "${prefab_file}" | grep "${GUID}" | wc -l`
    if [ "${COUNT_USED_FILE}" -ge 1 ]
    then
      echo "${prefab_file}" >> "${TMP_PREFABS_RESULT_FILES_NAME}"
    fi
  done
done

echo "==RESULT PREFABS=="
cat "${TMP_PREFABS_RESULT_FILES_NAME}"

/bin/rm "${TMP_RESOURCES_FILE_NAME}"
/bin/rm "${TMP_PREFABS_FILES_NAME}"
/bin/rm "${TMP_PREFABS_RESULT_FILES_NAME}"