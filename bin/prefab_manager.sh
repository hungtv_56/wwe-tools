#!/bin/bash
# Loading configuration
source .env

PREFAB_OPTION=${1}
case "$PREFAB_OPTION" in
  "find")
    NEED_TO_FIND_PREFAB_NAME=${2}
    cat $PREFAB_FILE_DIR | python -m json.tool | grep ${NEED_TO_FIND_PREFAB_NAME} | xargs
    exit
  ;;
  "edit")
    eval "${BLUEPRINT_EDITOR} ${PREFAB_FILE_DIR}"
  exit
  ;;
  "list_changes")
    cd ${CURRENT_WORKSPACE}
    # Format: YYYY-MM-DD HH:mm
    BEGIN_DATE="${2}"

    # Init data
    TMP_FILE_NAME_1=${HOME}"/prefab_tmp_1.txt"
    TMP_FILE_NAME_2=${HOME}"/prefab_tmp_2.txt"
    TMP_FILE_NAME_3=${HOME}"/prefab_tmp_3.txt"

    if [ -f ${TMP_FILE_NAME_1} ];
    then
      /bin/rm ${TMP_FILE_NAME_1}
    fi

    if [ -f ${TMP_FILE_NAME_2} ];
    then
      /bin/rm ${TMP_FILE_NAME_2}
    fi

    if [ -f ${TMP_FILE_NAME_3} ];
    then
      /bin/rm ${TMP_FILE_NAME_3}
    fi

    # List all file changes
    touch ${TMP_FILE_NAME_1}
    for i in `git log --after="${BEGIN_DATE}" | grep "^commit" | cut -d' ' -f2`
    do
      git show --pretty="" --name-only ${i} | grep ".prefab$" | grep "/_hirez/" >> ${TMP_FILE_NAME_1}
    done
    cat ${TMP_FILE_NAME_1} | sort | uniq > ${TMP_FILE_NAME_2}
    /bin/rm ${TMP_FILE_NAME_1}

    # Check if this file is exists
    while IFS='' read -r line || [[ -n "$line" ]]; do
        if [ -f "$line" ]
        then
          echo "${line}" >> "${TMP_FILE_NAME_3}"
        fi
    done < ${TMP_FILE_NAME_2}
    /bin/rm ${TMP_FILE_NAME_2}

    # Show the results
    cat "${TMP_FILE_NAME_3}" | grep -v "ODB"

    # Remove the files
    /bin/rm "${TMP_FILE_NAME_3}"
  exit
  ;;
esac