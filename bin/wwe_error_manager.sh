#!/bin/bash
# Loading configuration
source .env

PREFAB_OPTION=${1}
case "$PREFAB_OPTION" in
  "open")
    open "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/"
    exit
  ;;
  "rm")
    cd "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/"
    /bin/rm *.html
  exit
  ;;
esac