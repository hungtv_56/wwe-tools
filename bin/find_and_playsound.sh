#!/bin/bash
source .env

SFX_NAME=${2}
SFX_DIR=`find ${CURRENT_WORKSPACE}/Assets/_hirez/Audio -type f | grep "${SFX_NAME}$"`
echo "Playing: ${SFX_DIR}"
afplay "${SFX_DIR}"