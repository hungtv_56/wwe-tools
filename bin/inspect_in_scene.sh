#!/bin/bash
# Loading configuration
source .env

SCENE_DIR="${1}"
FILES_DIR="${2}"
GUID_FILE="${HOME}/guids.txt"
AUDIO_GUID_FILE="${HOME}/audio_guids.txt"

echo "SCENE_DIR: ${SCENE_DIR}"
echo "FILES_DIR: ${FILES_DIR}"
echo "AUDIO_GUID_FILE: ${AUDIO_GUID_FILE}"

# List all game prefab Guids
PREFAB_FOLDER="Prefabs:Resources:VFX"
IFS=': ' read -r -a PREFAB_DIRECTORY <<< "${PREFAB_FOLDER}"

# List All File Guids
if [ -e "${GUID_FILE}" ]
then
  /bin/rm "${GUID_FILE}"
fi

for DIRECTORY in "${PREFAB_DIRECTORY[@]}"
do
  DIRECTORY="${CURRENT_WORKSPACE}/Assets/_hirez/$DIRECTORY"
  echo "${DIRECTORY}"
  find ${DIRECTORY} -type f | grep ".meta$" | while read META_FILE
  do
    META_FILE_GUID=`cat "${META_FILE}" | grep "^guid: " | cut -d' ' -f2`
    echo "${META_FILE}:${META_FILE_GUID}" >> "${GUID_FILE}"
  done
done

# # List All File Guids
# if [ -e "${AUDIO_GUID_FILE}" ]
# then
#   /bin/rm "${AUDIO_GUID_FILE}"
# fi
# cd "${FILES_DIR}"

# for i in `ls "${FILES_DIR}" | grep ".meta$"`
# do
#   cat "${i}" | grep "^guid: " | cut -d' ' -f2 >> "${AUDIO_GUID_FILE}"
# done