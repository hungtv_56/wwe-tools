#!/bin/bash
# Loading configuration
source .env

OPTION="${1}"

# open "${CHAMPIONS_DST_FILE}Champions"
cd "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/Contents/"

if [ "${OPTION}" = "-n" ]
then
  /bin/rm -rf blueprints
fi

if [ ! -d "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/Contents/blueprints" ]; then
  unzip -q "${CHAMPIONS_DST_FILE}${PRODUCT_NAME}/Contents/blueprints.zip"
fi

eval "\"${BLUEPRINT_EDITOR}\" ."