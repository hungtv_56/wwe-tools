#!/bin/bash
# Loading configuration
source .env

FILE_DIR=${1}
if [ -z "$FILE_DIR" ]
then
  echo "wwe inspect <file_path>"
  exit
fi
echo "Searching..."

META_FILE_DIR=$FILE_DIR".meta"
# Get GUID
FILE_GUID=`cat "$META_FILE_DIR" | grep "^guid: " | cut -d' ' -f2`
wwe find $FILE_GUID

echo "Completed!"