## Installation

### Install Homebrew
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

### Install JQ - Json Parser
```
$ brew install jq
```

### Install Oh My ZSH
`$ sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`

or 

`$ sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"`

### Install Sublime Text

[https://www.sublimetext.com/3](https://www.sublimetext.com/3)

### Install WWE Tools
`$ git clone git@bitbucket.org:hungtv_56/wwe-tools.git`

`$ cd wwe-tools`

`$ chmod u+x install.sh`

`$ ./install.sh`

### Done!